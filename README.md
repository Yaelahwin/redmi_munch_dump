# missi-user 12
12 SKQ1.211006.001 V13.0.3.0.SLMIDXM release-keys
- manufacturer: xiaomi
- platform: kona
- codename: munch
- flavor: missi-user
- release: 12
12
- id: SKQ1.211006.001
- incremental: V13.0.3.0.SLMIDXM
- tags: release-keys
- fingerprint: Redmi/munch/munch:12/RKQ1.200826.002/V13.0.3.0.SLMIDXM:user/release-keys
Redmi/munch/munch:12/RKQ1.200826.002/V13.0.3.0.SLMIDXM:user/release-keys
POCO/munch_id/munch:12/RKQ1.200826.002/V13.0.3.0.SLMIDXM:user/release-keys
POCO/munch_in/munch:12/RKQ1.200826.002/V13.0.3.0.SLMIDXM:user/release-keys
- is_ab: true
- brand: Redmi
- branch: missi-user-12
12-SKQ1.211006.001-V13.0.3.0.SLMIDXM-release-keys
- repo: redmi_munch_dump
